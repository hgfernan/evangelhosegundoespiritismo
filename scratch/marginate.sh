#! /bin/sh 

set -x

if [ $# -lt 1 ]; then
	echo "$0": Missing file name
	exit 1
fi

PARAGRAPHS="$1"

PREF=$( echo "${PARAGRAPHS}" | awk 'BEGIN{ FS="." }{ print $1 }' )
PREF=$( echo "${PREF}" | awk 'BEGIN{ FS="-" }{ print $1 }' )

EXT=$( echo "${PARAGRAPHS}" | awk 'BEGIN{ FS="." }{ print $2 }' )
FMTED="${PREF}"-fmted."${EXT}"

cat "${PARAGRAPHS}" | fmt --uniform --width=72 | tee "${FMTED}"

