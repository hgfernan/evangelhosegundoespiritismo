#! /bin/sh

if [ $# -lt 1 ]; then
	echo $1: Please inform the base
	exit 1 
fi

BASE=$1

BODY="${BASE}"-body.txt
PARAGRAPHS="${BASE}"-pars.txt
FORMATTED="${BASE}"-fmted.txt

# TODO confirm ${BODY} exists

cat "${BODY}" | ./break_pars.py | tee "${PARAGRAPHS}"

cat "${PARAGRAPHS}" | fmt --uniform --width=72 | tee "${FMTED}"

