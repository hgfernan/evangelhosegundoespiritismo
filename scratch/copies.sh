#! /bin/sh

set -x 

if [ $# -lt 1 ]; then
	echo "$0": ERROR Inform the input file
	exit 1 
fi

CP=$( which cp )
CP="${CP}" -v -p

INPUT="$1"
BASE=$( echo "${INPUT}" | awk 'BEGIN{ FS="."}{ print $1 }' )
BASE=$( echo "${BASE}" | awk 'BEGIN{ FS="-"}{ print $1 }' )

EXT=$( echo "${1}" | awk 'BEGIN{ FS="."}{ print $2 }' )

BODY="${BASE}"-body."${EXT}"
CHAP="${BASE}"-chapters."${EXT}"

${CP} "${INPUT}" "${BODY}" 
${CP} "${INPUT}" "${CHAP}" 

