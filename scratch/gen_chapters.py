#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Generate LaTeX chapters from a list of Number-Hyphen-Text 

Created on Tue May  9 19:15:58 2023

@author: hilton
"""

import sys # argv, exit() 

from typing import List

def main(argv : List[str]) -> int:
    inp_f = sys.stdin
    
    for line in inp_f: 
        line = line.strip()
        
        fields = line.split(' ')
        
        chap_name = " ".join(fields[2:])
        
        print(f'\chapter{{ {chap_name} }}')
        print(f'% {line}\n')
        
    # Normal function termination
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))