#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun  9 09:32:50 2023

@author: hilton
"""

import sys # argv, exit() 

def main(argv : list[str]) -> int:
    inp_f = sys.stdin
    
    line_no = 0
    for line in inp_f:
        line_no += 1
        text = line.strip()
        text = text.strip(' ')
        # print(f'before find \'{text}\'')
        
        pos_dot = text.rfind('.')
        # print(f'pos_dot {pos_dot}')
        
        if pos_dot != -1:
            no_spc = False
            if text[pos_dot - 1] != ' ': 
                no_spc = True
                
            if text[pos_dot - 1] == '.':
                pos_dot -= 2
                # print(f'new pos_dot {pos_dot}')
                
            text = text[:pos_dot + 1].strip()
            # print(f'Spaced : \'{text}\'', file=sys.stderr)
            
            if no_spc: 
                text += ' '

        pos_spc = text.rfind(' ')
        if pos_spc == -1:            
            print(f'{line_no} : \'{text}\'', file=sys.stderr)
            continue

        # print(f'pos_spc {pos_spc}')
            
        line_out = text[:pos_spc].strip()
        print(line_out)

        # if line_no > 30:
        #     break
    
    # Normal function termination
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
