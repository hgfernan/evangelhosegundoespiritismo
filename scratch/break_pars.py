#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Created on Tue May  9 20:39:20 2023

@author: hilton
"""
import sys         # argv, exit() 
import string      # ascii_letters
import unicodedata # lookup()

from typing import List

def main(argv : List[str]) -> int:
    inp_f = sys.stdin
    
    uppercase_all = string.ascii_uppercase
    
    srch = 'LATIN CAPITAL LETTER A'
    uppercase_all += unicodedata.lookup(srch + ' with grave')
    uppercase_all += unicodedata.lookup(srch + ' with acute')
    uppercase_all += unicodedata.lookup(srch + ' with circumflex')
    uppercase_all += unicodedata.lookup(srch + ' with diaeresis')
    uppercase_all += unicodedata.lookup(srch + ' with ring above')
    
    # TODO Cedilla
    
    srch = 'LATIN CAPITAL LETTER E'
    uppercase_all += unicodedata.lookup(srch + ' with grave')   
    uppercase_all += unicodedata.lookup(srch + ' with acute')
    uppercase_all += unicodedata.lookup(srch + ' with circumflex')
    uppercase_all += unicodedata.lookup(srch + ' with diaeresis')
    
    srch = 'LATIN CAPITAL LETTER I'
    uppercase_all += unicodedata.lookup(srch + ' with grave')   
    uppercase_all += unicodedata.lookup(srch + ' with acute')
    uppercase_all += unicodedata.lookup(srch + ' with circumflex')
    uppercase_all += unicodedata.lookup(srch + ' with diaeresis')
    
    srch = 'LATIN CAPITAL LETTER O'
    uppercase_all += unicodedata.lookup(srch + ' with grave')   
    uppercase_all += unicodedata.lookup(srch + ' with acute')
    uppercase_all += unicodedata.lookup(srch + ' with circumflex')
    uppercase_all += unicodedata.lookup(srch + ' with diaeresis')
    
    srch = 'LATIN CAPITAL LETTER U'
    uppercase_all += unicodedata.lookup(srch + ' with grave')   
    uppercase_all += unicodedata.lookup(srch + ' with acute')
    uppercase_all += unicodedata.lookup(srch + ' with circumflex')
    uppercase_all += unicodedata.lookup(srch + ' with diaeresis')
    

    for line in inp_f: 
        line = line.strip()

        # TODO what about accented letters
        
        if len(line) == 0:
            continue 
        
        if line[0] in uppercase_all:
            print()
            
        print(f'{line}')
        
    # Normal function termination
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))