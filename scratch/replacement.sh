#! /bin/bash

set +x 

function next_set {
	export PREV=$( printf "${PREF}"'-%03d.tmp' ${IND} )
	export NEXT=$( printf "${PREF}"'-%03d.tmp'  $( echo $(( ${IND} + 1 )) )  )
	export IND=$(( ${IND} + 1 ))
}

if [ $# -lt 2 ]; then
	echo "$0" ERROR received "$#" parameters. Two are needed: 
	echo -e "\t The input filename and the prefix to the temporary files"
	exit 1
fi

export BASE="$1"
export PREF="$2" 

BODY=$( echo "${BASE}" | awk 'BEGIN{FS = "."}{ print $1}' )
EXT=$( echo "${BASE}" | awk 'BEGIN{FS = "."}{ print $2}' )
export REPD="${BODY}"-repd."${EXT}"

echo BASE="${BASE}"
echo PREF="${PREF}"
echo REPD="${REPD}"

export IND=0

if ( test -a "${BASE}"  ) ; then 
	echo File '"'"${BASE}"'"' will be used for input
	echo Temporary file prefix is '"'"${PREF}"'"'
else
	echo "$0" ERROR file '"'"${BASE}"'"' doesn"'"t exist
	exit 1
fi

next_set

cp "${BASE}" "${PREV}"

cat ${PREV} | sed 's/J esus/Jesus/g' > ${NEXT}

next_set 
cat ${PREV} | sed 's/Fr ancisco/Francisco/g' > ${NEXT}

next_set 
cat ${PREV} | sed 's/J uiz/Juiz/g' > ${NEXT}

next_set
cat ${PREV} | sed 's/\x20\x2d\x20/ -- /g' > ${NEXT}

next_set
cat ${PREV} | sed 's/ — / -- /g' > ${NEXT}

next_set
cat ${PREV} | sed 's/ —$/ --/g' > ${NEXT}

# OBS: \xe2\x80\x93 is a special sequence to change color to green
# OBS:     It is kept in the page header, just after the page break
# OBS:     It is ignored by Python UTF-8 processing.
# next_set
# cat ${PREV} | sed 's/\xe2\x80\x93//g' > ${NEXT}

# OBS: \xe2\x80\x95 is a special sequence, still unknown
# OBS:     It causes problems to LaTeX processing.
next_set
cat ${PREV} | sed 's/\xe2\x80\x95//g' > ${NEXT}

next_set
cat ${PREV} | sed 's/^— /-- /g' > ${NEXT}

next_set
cat ${PREV} | sed 's/VINH A/VINHA/g' > ${NEXT}


next_set
cat ${PREV} | sed 's/-Ias/las/g' > ${NEXT}

next_set
cat ${PREV} | sed 's/ü/u/g' > ${NEXT}

next_set
cat ${PREV} | sed 's/\.\.\./\\ldots\{\}/g' > ${NEXT}

next_set
cat ${PREV} | sed 's/\. \. \./\\ldots\{\}/g' > ${NEXT}

next_set
cat ${PREV} | sed 's/“/''`''`''/g' > ${NEXT}

next_set
SRCH='s/”/'"''"'/g'
cat ${PREV} | sed "${SRCH}" > ${NEXT}

next_set
cat ${PREV} | sed 's/ \xc2\xad / -- /g' > ${NEXT}

next_set
cat ${PREV} | sed 's/\xc2\xad/-/g' > ${NEXT}

next_set
cat ${PREV} | sed 's/\xcd\xbe/;/g' > ${NEXT}

next_set
cat ${PREV} | sed 's/J oão/João/g' > ${NEXT}

next_set
cat ${PREV} | sed 's/J udas/Judas/g' > ${NEXT}

next_set
cat ${PREV} | sed 's/Pedr o/Pedro/g' > ${NEXT}

next_set
cat ${PREV} | sed 's/J erusalém/Jerusalém/g' > ${NEXT}

next_set
cat ${PREV} | sed 's/J udéia/Judéia/g' > ${NEXT}

next_set
cat ${PREV} | sed 's/J ordão/Jordão/g' > ${NEXT}

next_set
cat ${PREV} | sed 's/J onas/Jonas/g' > ${NEXT}

next_set
cat ${PREV} | sed 's/Ver dade/Verdade/g' > ${NEXT}

next_set
cat ${PREV} | sed 's/q uem/quem/g' > ${NEXT}

next_set
cat ${PREV} | sed 's/^\- /-- /g' > ${NEXT}

next_set
cat ${PREV} | sed 's/\xe2\x80\x94/--/' > ${NEXT}

next_set
cat ${PREV} | sed 's/\xe2\x80\x93/--/' > ${NEXT}

next_set
cat ${PREV} | sed 's/JesusCristo/Jesus-Cristo/' > ${NEXT}

next_set
cat ${PREV} | sed 's/boavontade/boa-vontade/' > ${NEXT}

mv ${NEXT} "${REPD}"
