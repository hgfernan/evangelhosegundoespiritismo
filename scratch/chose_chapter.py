#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Selection of item in ESE
Created on Sun Oct 20 02:55:15 2024

@author: hilton
"""

import sys # argv, exit()
import os.path # exists()

# from typing import List, Dict, OrderedDict
from typing import List, Dict

import numpy  as np
import pandas as pd

def main(argv : List[str]) -> int:
    freqs_csv : str = 'ese_freqs.csv'
    # TODO get program parameters
    # TODO input chapter lists
    
    counts = pd.read_csv('ese_items.csv')
    
    # TODO input the frequency lists
    if os.path.exists(freqs_csv):
        freqs = pd.read_csv(freqs_csv)
    
    else: 
        freqs = pd.DataFrame({'Chapter' : [], 'Item' : [], 'Frequency' : []})
   
    freqs_dict : Dict[int, str] = { }
    for ind, row in freqs.iterrows(): 
        key = f"{row['Chapter']:02d}-{row['Item']:02d}"
        freqs_dict[key] = row['Frequency']
    
    # TODO select a chapter and item
    rng = np.random.default_rng()
    
    frst_chap = counts['Chapter'].min()
    last_chap = counts['Chapter'].max()
    chap = rng.integers(frst_chap, last_chap + 1, 1)[0]
    
    frst_item = 1
    last_item = counts.iloc[chap - frst_chap]['Last_item'].max()
    item = rng.integers(frst_item, last_item + 1,  1)[0]
    
    print(chap, item)
    
    # TODO add chapter and index to the frequency table
    key = f"{chap:02d}-{item:02d}"
    freqs_dict[key] = freqs_dict.get(key, 0) + 1
    
    # TODO write down the frequency lists
    chaps : List[int] = []
    items : List[int] = []
    freqs : List[int] = []
    ord_keys = sorted(freqs_dict)
    
    for key in ord_keys:
        fields = key.split('-')
        chaps.append(int(fields[0]))
        items.append(int(fields[1]))
        freqs.append(freqs_dict[key])
    
    freqs = pd.DataFrame({'Chapter' : chaps, 'Item' : items, 
                          'Frequency' : freqs})
    
    freqs.to_csv(freqs_csv, index=False)
    
    # Normal function termination
    return 0

if __name__ == '__main__': 
    sys.exit(main(sys.argv))
