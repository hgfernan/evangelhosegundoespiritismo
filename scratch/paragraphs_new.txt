
% 23. Vinde, todos vós que desejais crer. Acorrem os Espíritos

\stepcounter{akitem}

{\theakitem}. Vinde, todos vós que desejais crer. Acorrem os
Espíritos celestes, e vêm anunciar-vos grandes coisas! Deus,
meus filhos, abre os seus tesouros, para vos distribuir os
seus benefícios. Homens incrédulos! Se soubésseis como a fé
beneficia o coração, e leva a alma ao arrependimento e à prece! A
prece. Ah! Como são tocantes as palavras que se desprendem dos
lábios na hora da prece! Porque a prece é o orvalho divino, que
suaviza o excessivo calor das paixões. Filha predileta da fé,
leva-nos ao caminho que conduz a Deus. No recolhimento e na solidão
encontrai-vos com Deus; e para vós o mistério se desfaz, porque
Ele se revela.  Apóstolos do pensamento, a verdadeira vida se abre
para vós. Vossa alma se liberta da matéria e se lança pelos mundos
infinitos etéreos, que a pobre Humanidade desconhece.

Marchai, marchai, pelos caminhos da prece, e ouvireis a voz dos
Anjos! Que harmonia! Não são mais os ruídos confusos e as vozes
gritantes da Terra. São as liras dos Arcanjos, as vozes doces e
meigas dos Serafins, mais leves que as brisas da manhã, quando
brincam nas ramagens dos vossos arvoredos. Com que alegria então
marchais! Vossa linguagem terrena não poderá exprimir jamais essa
ventura, que vos impregna por todos os poros, tão viva e refrescante
é a fonte em que bebemos através da prece! Doces vozes, inebriantes
perfumes, que a alma ouve e aspira, quando se lança, pela prece,
a essas esferas desconhecidas e habitadas! São divinas todas as
aspirações, quando livres dos desejos carnais.  Vós também, como
Cristo, orai, carregando a vossa cruz para o Gólgota, para o vosso
Calvário. Levai-a, e sentireis as doces emoções que lhe passava
nela alma, embora carregaste o madeiro infamante. Sim, porque ele ia
morrer, mas para viver a vida celestial, na morada do Pai!
