#! /bin/sh

if [ $# -lt 1 ]; then
	echo $1: Please inform the base
	exit 1 
fi

BASE=$1

TEXT="${BASE}"-chapters.txt
LATEX="${BASE}"-chapters.tex

# TODO confirm ${TEXT} exists

cat "${TEXT}" | ./prep_chapters.py | ./gen_chapters.py | tee "${LATEX}" 

