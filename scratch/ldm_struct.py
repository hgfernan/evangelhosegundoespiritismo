#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  9 20:07:01 2024

@author: user
"""

import sys # argv, exit() 

def main() -> int:
    inp_f = open('ldm_struct.txt', 'r')
    
    for line in inp_f:
        stripped = line.strip()
        blank = (len(stripped) == 0)
    
        if blank:
            continue
        
        if line[0] != ' ':
            print(f'\n\n\\chapter{{ {stripped} }}\n')
            
            continue 
        
        print(f'\\subsection*{{ {stripped} }}')
        print(f'\\addcontentsline{{toc}}{{section}}{{ {stripped} }}\n')
    
    # Normal function termination
    return 0

if __name__ == '__main__':
    sys.exit(main())
