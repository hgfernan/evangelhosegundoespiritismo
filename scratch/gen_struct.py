#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Generation of LaTeX structure from Liz Bittar's HTML' 
Created on Wed May 15 09:38:36 2024

@author: hilton
"""

import sys # argv, exit()

from typing import List 


def gen_chapter(line : str) ->str:
    result = ''
    
    work = line 
    if work[0:8] == 'Capítulo':
        fields = work.split('–')
        work = fields[1].lstrip(' ')
        
    work = work.capitalize()
    
    result = '\\chapter{' + work + '}\n'
    
    # Normal function termination    
    return result 


def gen_section(line : str) ->str:
    work : str = line.lstrip()
    result : str = ''

    result = '\\section*{' + work + '}\n'
    result += '\\addcontentsline{toc}{section}'
    result += '{' + work + '}\n\n'
    
    # Normal function termination    
    return result 


def gen_subsection(line : str) ->str:
    work : str = line.lstrip()
    result : str = ''

    result = '\\subsection*{' + work + '}\n'
    result += '\\addcontentsline{toc}{subsection}'
    result += '{' + work + '}\n\n'
    
    # Normal function termination    
    return result 


def main(argv : List[str]) -> int:
    inp_f = sys.stdin
    line : str = ''
    
    if len(argv) > 1:
        try:
            inp_f = open(argv[1], 'r')
        
        except Exception as exc:
            line = type(exc).__name__
            line += ': ' + str(exc)
            
            print(line)
            
            # Return to indicate failure
            return 1
        
    for line in inp_f:
        line = line.rstrip()

        if len(line) == 0:
            continue
        
        # OBS If it's a chapter
        if line[0] != ' ':
            text = gen_chapter(line)
            
        # OBS If it's a subsection
        elif line[0:8] == 8*' ':
            text = gen_subsection(line)
            
        # OBS If it's a section
        elif line[0:4] == 4*' ':
            text = gen_section(line)
        
        else:
            text = line
            
        print(text)
            
    
    # Normal function termination
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
